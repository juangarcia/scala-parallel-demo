## Futures, Parallel Collections, and the Execution Context

### Scala Threading
Scala out of the box provides support for working with threaded code in an ergonomic fashion. It does not require much thought into thread pools, thread management, writing the call back code, and other threading details that a Java author may be used to dealing with until you want to do something different. For most intents and purposes if you just want parallel code you may not need to do anything other than use the Scala `Future`.

### Future API
So what does the code look like?

``` scala
def futureImpl(): Unit = {
  // used to here to constrain the global context to this method
  import scala.concurrent.ExecutionContext.Implicits.global

  def wrapper(): Future[List[Unit]] = {
    val intervalledMessage = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)

    val list: List[Future[Unit]] = intervalledMessage.map(interval => {
      Future {
        while (true) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }
      }
    })

    Future.sequence(list)
  }

  Await.result(wrapper(), Duration.Inf)
}
```

Here we have a `List` of items that we want to process in an asynchronous fashion - using the `foreach` operator is not an option. To accomplish this we map each of the messages to a `Future` each result is added to a `List` and once all of our `Future`s are wired we place them on the execution path with `Future.sequence()`. We then wait for all `Future`s to complete execution with `Await.result()`.

The magic is contained with `import` statement it provides a `ExecutionContext` by default backed by a  `ForkJoinPool`  with the number of threads allocated to this pool defined by `Runtime.getRuntime.availableProcessors()`. Assuming you are running the JVM on bare metal on a typical server this may be 32 / 48 CPUs; on a typical desktop workstation 6 or 8 CPUs. When it comes to containers though this number will be based on the number of CPUs allocated. Docker for example allows one to limit a CPU using the `--cpus="1"` flag. When dealing with Spark `driverCores` and `executorCores` from Apache Livy are also a thing that may limit the default number of CPUs available to your JVM.

The previous is to say that if you allocate a single CPU to your JVM you may not be getting what you expected to get. If you have 6 tasks you want to execute concurrently and your JVM is on a Docker / Yarn container with only a single CPU allocated to it you have two options - increase the number of CPUs or define your own execution context. So let us try the latter.

``` scala
def forkJoinPoolFutureImpl(): Unit = {
  def wrapper(): Future[List[Unit]] = {
    val intervalledMessage = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)

    val list: List[Future[Unit]] = intervalledMessage.map(interval => {
      Future {
        while (true) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }
      }(Example.localEc)
    })

    Future.sequence(list)(implicitly, Example.localEc)
  }

  Await.result(wrapper(), Duration.Inf)
}


object Example {
  implicit val localEc: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4))
}
```

In the `Example` object we've defined an implicit `ExecutionContext` we did this to ensure new objects use the same `Executor`. We set a reasonable number for the number of threads. Finally, on the `Future` we set the `ExecutionContext` we created from a fixed thread pool `Future{}(Example.localEc)`. Now we can more accurately state how our application will behave regardless of environment.

### Parallel Collections API
Note that the previous statements also applies when working with the parallel collections from

``` scala
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0"
```

This piece of code does same set of work in example one. It is limited by the number of CPUs available to your JVM and if only one CPU is allocated this will process synchronously. The magic here is enabled by the `.par` call.

``` scala
import scala.collection.parallel.CollectionConverters._

def defaultParallelImpl(): Unit = {
  List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)
    .par
    .foreach(interval => {
      while (true) {
        println(s"Interval for $interval")
        Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
      }
    })
}
```

Each result from our `List` is executed in an asynchronous fashion up until the point that you have met the number of CPUs available to you. An advantage from the previous code is that you do not need to think about building a list, adding the things to the list, calling the `Future.sequence()` and doing the `Await.result()`. 

To match the second block code above we do some more work

``` scala
import scala.collection.parallel.CollectionConverters._
import scala.collection.parallel.ForkJoinTaskSupport

def forkJoinPoolParallelImpl(): Unit = {
  val numberList = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)
  val parallelNumbers = numberList.par
  parallelNumbers.tasksupport = new ForkJoinTaskSupport(Example.forkJoinPool)

  parallelNumbers.foreach(interval =>
    while (true) {
      println(s"Interval for $interval")
      Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
    }
  )
}


object Example {
  val forkJoinPool: ForkJoinPool = new ForkJoinPool(15)
}
```

A second parallel collection is built because an ergonomic API is missing from parallel collections. We set a `ForkJoinTaskSupport` on the parallel API with our `ForkJoinPool` backing it. We then iterate on our list.

### Order of executions
One thing that may be interesting to note is when are your tasks actually running? Take this piece of code for example

``` scala
def defaultParallelImplWithFollowUp(): Unit = {
  List(15, 20, 25, 30, 35)
    .par
    .map(interval => {
      for (a <- 1 to 3) {
        println(s"Interval for $interval")
        Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
      }

      interval
    })
    .foreach(interval => {
      println(s"Previous execution finished for $interval")
    })
}
```

It has a list of 5 items. It creates a default parallel collection and maps over the results. For each result it will sleep at most 3 times for what ever number of seconds is the interval period. Afterwards we iterate over the results with a `foreach`. For better or worse a parallel map is blocking task. Execution does not flow to the next `foreach`, `map`, `join`, etc until every item has been iterated over.

What does this look like when using futures?

``` scala
def futureImplWithFollowUp(): Unit = {
  // used to here to constrain the global context to this method
  import scala.concurrent.ExecutionContext.Implicits.global

  def wrapper(): Future[List[Int]] = {
    val intervalledMessage = List(15, 20, 25, 30)

    val list: List[Future[Int]] = intervalledMessage.map(interval => {
      Future {
        for (a <- 1 to 3) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }

        interval
      }
    })

    Future.sequence(list)
  }

  // your futures are doing nothing here! Don't believe me? Uncomment me
  // Thread.sleep(TimeUnit.SECONDS.toMillis(60))

  val results = Await.result(wrapper(), Duration.Inf)
  results.foreach(interval => {
    println(s"Previous execution finished for $interval")
  })
}
```

Similar to the parallel code the returned values from your list of futures are not available until execution completes for everything on the list. Also, your futures do nothing until called with `Await.result()` which seems contradictory to what a Future is.

So how do we obtain fire and forget if we want that?

``` scala
def futureImplWithOnComplete(): Unit = {
  // used to here to constrain the global context to this method
  import scala.concurrent.ExecutionContext.Implicits.global

  def internalCallBack(interval: Integer): Unit = {
    println(s"Previous execution finished for $interval")
  }

  val intervalledMessage = List(5, 10, 15, 20)
  intervalledMessage.foreach(interval => {
    val thing = Future {
      for (a <- 1 to 3) {
        println(s"Interval for $interval")
        Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
      }
      interval
    }

    thing.onComplete {
      case Success(value) => internalCallBack(value)
      case Failure(exception) => println(exception)
    }
  })
  
  // this runs the second the foreach is completed.
  println("do all the things!")

  // your futures are still running even as some other operation
  // may be occurring
  Thread.sleep(TimeUnit.SECONDS.toMillis(25))
}
```

We somewhat build on the previous example. The `Future.sequence()` and `Await.result()` are no longer a part of the picture. Instead we leverage `Future.onComplete()` for demonstration purposes we added an internal function to our function... to actually view the results of this we had to sleep the JVM otherwise the JVM exits with no regard to what is still running.

Depending on what you actually need your code to do you may need to fallback on working with the `Future` API. If you just need to iterate on a `List` do a computation on all of them and return the modified result parallel collections are definitely more readable in my opinion.

### Thread pool things to consider
#### Thread pool exhaustion

In the examples that we elected to use our own thread pool we placed the thread pools on the object. Both the `ForkJoinPool` and `ExecutorService` have the `.shutdown()` call to close down the pool. Creating new thread pools without closing them down will eventually either a) run your JVM out of memory b) run your JVM out of available threads. In Java what we would be after is a `static` thread pool tied to the class. To make up for this we use the scala object.

#### My JVM never exits

Creating an `ExecutorService` to run something in parallel, having the thing complete, and proceeding to application shutdown will not occur until `shutdown()` is invoked.

#### Threads vs CPUs

The result of the creating custom thread pools may result in a JVM with one CPU potentially operating many threads and that may be okay! Is your application CPU bound? If it is then you really do need more CPUs or a code optimization. Is your application not CPU constrained and is instead just blocking waiting on IO? Then the previous solution may be for you. Spinning up many threads to do a set of work is setting your application to perform a time stealing operation on your CPU which is something that all threads already to do begin with.

Additionally, for the most part idle threads are not system impacting kept within a reasonable limit.

#### With a Spark context

JVM threads are very likely NOT what you after in Spark. In Spark what you probably want to work towards too is being able to spin up many Spark executors that enable horizontal scaling where as JVM threading is closer to vertical scaling.

### TL;DR - What should I use?
Use parallel collections if you already have a `List` and do not need call backs or encapsulate what would be your callback as a part of your parallel iteration. Use `Future` when you just have a block of code you want to execute on and for some reason this block code is not a `List`.

### References
Sites live as of Jan 19, 2020 at 8:28 PM

1. [Futures and Promises | Scala Documentation](https://docs.scala-lang.org/overviews/core/futures.html)
2. [Livy Docs - REST API](https://livy.incubator.apache.org/docs/latest/rest-api.html)
3. [Runtime options with Memory, CPUs, and GPUs | Docker Documentation](https://docs.docker.com/config/containers/resource_constraints/)
4. [SIP-22 - Async | Scala Documentation](https://docs.scala-lang.org/sips/async.html)

<!-- #java/scala -->
