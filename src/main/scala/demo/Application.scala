package demo

import java.util.concurrent.{Executors, ForkJoinPool, TimeUnit}

import scala.collection.parallel.CollectionConverters._
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
 * <p></p>
 *
 * @author Juan Garcia
 * @since 2020-01-19
 */
object Application {


  def main(string: Array[String]): Unit = {
    println(s"Available CPUs ${Runtime.getRuntime.availableProcessors()}")
    futureImplWithOnComplete()
  }

  /**
   * Uses the default implementation for parallelism which relies on the
   * scala implicit [[ExecutionContext]] and is limited by the number of
   * visible to CPUs on the JVM
   */
  def defaultParallelImpl(): Unit = {
    List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)
      .par
      .foreach(interval => {
        while (true) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }
      })
  }

  def defaultParallelImplWithFollowUp(): Unit = {
    List(15, 20, 25, 30, 35)
      .par
      .map(interval => {
        for (a <- 1 to 3) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }

        interval
      }).par
      .foreach(interval => {
        println(s"Previous execution finished for $interval")
      })
  }

  /**
   * Uses the [[ForkJoinPool]] created on the object as the backing pool
   * for the parallel execution of all items in a [[List]]
   */
  def forkJoinPoolParallelImpl(): Unit = {
    val numberList = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)
    val parallelNumbers = numberList.par
    parallelNumbers.tasksupport = new ForkJoinTaskSupport(Example.forkJoinPool)

    parallelNumbers.foreach(interval =>
      while (true) {
        println(s"Interval for $interval")
        Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
      }
    )
  }

  /**
   * Uses the default [[ExecutionContext]] which is backed by a default
   * [[ForkJoinPool]] implementation with a number of threads that is
   * visible to the JVM.
   */
  def futureImpl(): Unit = {
    // used to here to constrain the global context to this method
    import scala.concurrent.ExecutionContext.Implicits.global

    def wrapper(): Future[List[Unit]] = {
      val intervalledMessage = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)

      val list: List[Future[Unit]] = intervalledMessage.map(interval => {
        Future {
          while (true) {
            println(s"Interval for $interval")
            Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
          }
        }
      })

      Future.sequence(list)
    }

    Await.result(wrapper(), Duration.Inf)
  }

  /**
   * Uses the default [[ExecutionContext]]. The results from the [[Future]]
   * are to be used in another operation. However, like the parallel implementation
   * as a single item complete it is not usable until all items complete.
   */
  def futureImplWithFollowUp(): Unit = {
    // used to here to constrain the global context to this method
    import scala.concurrent.ExecutionContext.Implicits.global

    def wrapper(): Future[List[Int]] = {
      val intervalledMessage = List(15, 20, 25, 30)

      val list: List[Future[Int]] = intervalledMessage.map(interval => {
        Future {
          for (a <- 1 to 3) {
            println(s"Interval for $interval")
            Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
          }

          interval
        }
      })

      Future.sequence(list)
    }

    // your futures are doing nothing here! Don't believe me? Uncomment me
    // Thread.sleep(TimeUnit.SECONDS.toMillis(60))

    println("do a thing")
    val results = Await.result(wrapper(), Duration.Inf)
    results.foreach(interval => {
      println(s"Previous execution finished for $interval")
    })
    println("do more things")
  }

  /**
   * Uses the default [[ExecutionContext]]. We iterate on the [[List]] intervals
   * with no regard to preserving the results. Once a [[Future]] is completed
   * the [[Future.onComplete()]] is used to immediately fire off some other
   * operation. This code is truly asynchronous there is so much fire and forget
   * here that we had to sleep the JVM otherwise processing would not continue.
   */
  def futureImplWithOnComplete(): Unit = {
    // used to here to constrain the global context to this method
    import scala.concurrent.ExecutionContext.Implicits.global

    def internalCallBack(interval: Integer): Unit = {
      println(s"Previous execution finished for $interval")
    }

    val intervalledMessage = List(5, 10, 15, 20)

    intervalledMessage.foreach(interval => {
      val thing = Future {
        for (a <- 1 to 3) {
          println(s"Interval for $interval")
          Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
        }

        interval
      }

      thing.onComplete {
        case Success(value) => internalCallBack(value)
        case Failure(exception) => println(exception)
      }
    })

    // this runs the second the foreach is completed.
    println("do all the things!")

    // your futures are still running even as some other operation
    // may be occurring
    Thread.sleep(TimeUnit.SECONDS.toMillis(25))
  }

  /**
   * Uses a custom [[ExecutionContext]] backed by fixed thread pool. Use of
   * this means of parallelism requires that each [[Future]] be told of what
   * [[ExecutionContext]] to be used. Additionally, the sequence call requires
   * to be told how to build the collection and the [[ExecutionContext]] to use.
   */
  def forkJoinPoolFutureImpl(): Unit = {
    def wrapper(): Future[List[Unit]] = {
      val intervalledMessage = List(15, 20, 25, 30, 35, 40, 45, 50, 55, 60)

      val list: List[Future[Unit]] = intervalledMessage.map(interval => {
        Future {
          while (true) {
            println(s"Interval for $interval")
            Thread.sleep(TimeUnit.SECONDS.toMillis(interval))
          }
        }(Example.localEc)
      })

      Future.sequence(list)(implicitly, Example.localEc)
    }

    Await.result(wrapper(), Duration.Inf)
  }
}

object Example {
  implicit val localEc: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(15))
  val forkJoinPool: ForkJoinPool = new ForkJoinPool(15)
}
